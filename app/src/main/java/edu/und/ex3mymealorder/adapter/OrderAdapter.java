package edu.und.ex3mymealorder.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import edu.und.ex3mymealorder.R;
import edu.und.ex3mymealorder.database.DatabaseInfo;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {
    private List<DatabaseInfo> getDataList;
    ArrayList<DatabaseInfo> combo_list = new ArrayList<>();
    ArrayList<String> price_list = new ArrayList<>();
    ArrayList<String> food_name_list = new ArrayList<>();

    private String[] mDataset;


    Context context;

    public OrderAdapter(Context mainActivity, List<DatabaseInfo> food_list) {
        this.context = mainActivity;
        this.getDataList = food_list;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView food;


        public MyViewHolder(View view) {
            super(view);
            food = view.findViewById(R.id.foodname);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_single, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final DatabaseInfo databaseInfo = getDataList.get(position);
        holder.food.setText(databaseInfo.getUniqueName());





    }


    @Override
    public int getItemCount() {
        return getDataList.size();
    }





}
