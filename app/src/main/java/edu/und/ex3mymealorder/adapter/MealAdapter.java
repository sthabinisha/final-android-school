package edu.und.ex3mymealorder.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import edu.und.ex3mymealorder.MainActivity;
import edu.und.ex3mymealorder.R;
import edu.und.ex3mymealorder.database.DatabaseInfo;


public class MealAdapter  extends RecyclerView.Adapter<MealAdapter.MyViewHolder> {
    private List<DatabaseInfo> getDataList;
    ArrayList<DatabaseInfo> combo_list = new ArrayList<>();
    ArrayList<String> price_list = new ArrayList<>();
    ArrayList<String> food_name_list = new ArrayList<>();

    private String[] mDataset;


    Context context;

    public MealAdapter(Context mainActivity, List<DatabaseInfo> food_list) {
        this.context = mainActivity;
        this.getDataList = food_list;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView food;
        public TextView price;

        public CheckBox checkBox;

        public MyViewHolder(View view) {
            super(view);
            food = view.findViewById(R.id.foodname);
            price = view.findViewById(R.id.foodPrice);
            checkBox = view.findViewById(R.id.checkbox_food);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_recycle, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final DatabaseInfo databaseInfo = getDataList.get(position);
        holder.food.setText(databaseInfo.getUniqueName());
        holder.price.setText(databaseInfo.getPRICE().toString());


        holder.price.addTextChangedListener(new TextWatcher() {
            private  int position;

            public void updatePosi(int position) {
                this.position = position;
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                databaseInfo.setEditTextValue(holder.price.getText().toString());



            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
                //databaseInfo.setSelected(isChecked);
                int id = buttonView.getId();
                if (id == R.id.checkbox_food) {
                    if (holder.checkBox.isChecked()) {
                        databaseInfo.setSelected(isChecked);


                    } else {
                        databaseInfo.setSelected(false);

                    }

                    Toast.makeText(context, databaseInfo.getUniqueName(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return getDataList.size();
    }

    public ArrayList<DatabaseInfo> getSelectedList() {
        combo_list.clear();

        for (int i = 0; i < getDataList.size(); i++) {
            if (getDataList.get(i).isSelected()) {
                combo_list.add(getDataList.get(i));
                price_list.add(getDataList.get(i).getEditTextValue());
                food_name_list.add(getDataList.get(i).getUniqueName());


            }else{
                combo_list.remove(getDataList.get(i));
            }

        }
        return combo_list;
    }



}
