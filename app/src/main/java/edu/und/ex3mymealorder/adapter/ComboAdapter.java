package edu.und.ex3mymealorder.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import edu.und.ex3mymealorder.R;
import edu.und.ex3mymealorder.database.DatabaseInfo;
import edu.und.ex3mymealorder.fragment.AllComboFragment;
import edu.und.ex3mymealorder.fragment.ComboDetails;


public class ComboAdapter extends RecyclerView.Adapter<ComboAdapter.MyViewHolder> {
    private List<DatabaseInfo> getDataList;
    ArrayList<DatabaseInfo> combo_list = new ArrayList<>();
    private static RadioButton lastChecked = null;
    private static int lastCheckedPos = 0;
    String textPrice;

    Context context;
    AllComboFragment currentFragment;

    public ComboAdapter(Context comboData, List<DatabaseInfo> food_list, AllComboFragment allComboFragment) {
        this.context = comboData;
        this.getDataList = food_list;
        this.currentFragment = allComboFragment;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView food, foodID;
        public TextView price;

        public RadioButton radioButton;

        public MyViewHolder(View view) {
            super(view);
            foodID= view.findViewById(R.id.foodID);
            food = view.findViewById(R.id.foodname);
            price = view.findViewById(R.id.foodPrice);
            radioButton = view.findViewById(R.id.radio_food);

        }
    }

    @Override
    public ComboAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.combo_single_item, parent, false);

        return new ComboAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ComboAdapter.MyViewHolder holder, final int position) {
        final DatabaseInfo databaseInfo = getDataList.get(position);
        holder.foodID.setText(databaseInfo.getUniqueId().toString());
        holder.price.setText(databaseInfo.getPRICE().toString());

        holder.food.setText(databaseInfo.getCOMBO().toString());
        holder.radioButton.setChecked(databaseInfo.isSelected());
        holder.radioButton.setTag(new Integer(position));

        //makeTextViewHyperlink(holder.food);
        makeTextViewHyperlink(holder.foodID);

        holder.price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                textPrice = editable.toString();
                System.out.println(textPrice);

            }
        });
        holder.foodID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                currentFragment.callnextFrag(databaseInfo.getUniqueId());

            }
        });
        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RadioButton radioButton = (RadioButton)view;
                int ClickedPos = ((Integer)radioButton.getTag()).intValue();
                if(radioButton.isChecked()){
                    if(lastChecked!= null){
                        lastChecked.setChecked(false);
                        databaseInfo.setSelected(true);


                    }
                    lastChecked = radioButton;
                    lastCheckedPos = ClickedPos;
                }else{
                    lastChecked = null;
                    databaseInfo.setSelected(true);
                    textPrice = holder.price.getText().toString();

                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return getDataList.size();
    }

    public  int getSelected(){
        int selected_combo;


        return lastCheckedPos;
    }
    public static void makeTextViewHyperlink( TextView tv ) {

        SpannableStringBuilder ssb = new SpannableStringBuilder( );

        ssb.append( tv.getText( ) );

        ssb.setSpan( new URLSpan("#"), 0, ssb.length(),

                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );

        tv.setText( ssb, TextView.BufferType.SPANNABLE );

    }


}
