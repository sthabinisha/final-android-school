package edu.und.ex3mymealorder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import edu.und.ex3mymealorder.adapter.ComboAdapter;

public class DatabaseHelper  extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    float price = 0;
    float price1 = 0;
    float lowPrice = 0;
    float comboPrice = 0;

    SQLiteDatabase db;
    ArrayList<String> compareName = new ArrayList<>();
    DatabaseInfo info;
    String maxPriceName = "";
    Float maxPriceValue;


    // Database Name
    private static final String DATABASE_NAME = "food_order.db";


    ArrayList<DatabaseInfo> comboList = new ArrayList<>();
    String combo_treat, comboNoComma, returnstringFood;
    float sum = 0;
    ComboAdapter adapter;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open( ) throws SQLException {

         db = this.getWritableDatabase();

    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create food table
        db.execSQL(DatabaseInfo.CREATE_FOOD_TABLE);
        // create combo table
        db.execSQL(DatabaseInfo.CREATE_MEAL_TABLE);
        db.execSQL(DatabaseInfo.CREATE_FOOD_COMBO_TABLE);


    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.FOOD_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.COMBO_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseInfo.FOOD_COMBO);


        // Create tables again
        onCreate(db);
    }

    public boolean insertMenu(String foodname, String price) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(DatabaseInfo.UNIQUE_NAME, foodname);
        values.put(DatabaseInfo.PRICE, price);


        // insert row
        db.insert(DatabaseInfo.FOOD_TABLE, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return true;
    }

    public void insertComboMeal(ArrayList<DatabaseInfo> foonameSelected, String price) {
        this.comboList = foonameSelected;
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase dbr = this.getReadableDatabase();


        ContentValues values = new ContentValues();
        StringBuilder sbString = new StringBuilder("");

        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        for (int i = 0; i < comboList.size(); i++) {
            sbString.append(foonameSelected.get(i).getUniqueName()).append(",");
        }
        combo_treat = sbString.toString();


        try {
            combo_treat = combo_treat.substring(0, combo_treat.length() - 1);
            String query = "select * from " + DatabaseInfo.COMBO_TABLE + " where " + DatabaseInfo.COMBO + " = '" + combo_treat + "'";
            Cursor cursor1 = dbr.rawQuery(query, null);
            if (cursor1.getCount() <= 0) {

                values.put(DatabaseInfo.COMBO, combo_treat);
                values.put(DatabaseInfo.PRICE, price);


                // insert row
                db.insert(DatabaseInfo.COMBO_TABLE, null, values);

            }
            String selectFromComboQuery = "SELECT " + DatabaseInfo.UNIQUE_ID + " FROM " + DatabaseInfo.COMBO_TABLE +
                    "  where " + DatabaseInfo.COMBO + "='" + combo_treat + "'";
            Cursor cursor = db.rawQuery(selectFromComboQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    int unique_id = cursor.getInt(cursor.getColumnIndex(DatabaseInfo.UNIQUE_ID));
                    insertConTable(foonameSelected, unique_id);
                } while (cursor.moveToNext());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // close db connection
        db.close();
        //insertConTable(id, foodname);


        // return newly inserted row id
        //return true;
    }


    public boolean insertConTable(ArrayList<DatabaseInfo> foonameSelected, int unique_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        this.comboList = foonameSelected;
        for (int i = 0; i < comboList.size(); i++) {
            values.put(DatabaseInfo.UNIQUE_ID, unique_id);
            values.put(DatabaseInfo.UNIQUE_NAME, foonameSelected.get(i).getUniqueName());

            db.insert(DatabaseInfo.FOOD_COMBO, null, values);


        }


        // insert row

        // close db connection
        db.close();


        // return newly inserted row id
        return true;
    }

    public List<DatabaseInfo> getMenu() {
        List<DatabaseInfo> menu = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DatabaseInfo.FOOD_TABLE + " ORDER BY " +
                DatabaseInfo.UNIQUE_NAME + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo info = new DatabaseInfo();
                info.setUnique_name(cursor.getString(cursor.getColumnIndex(DatabaseInfo.UNIQUE_NAME)));
                info.setPrice(cursor.getFloat(cursor.getColumnIndex(DatabaseInfo.PRICE)));

                menu.add(info);
            } while (cursor.moveToNext());
        }
        // close db connection
        db.close();

        // return menu list
        return menu;
    }

    public List<DatabaseInfo> getCombo() {
        List<DatabaseInfo> menu = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DatabaseInfo.COMBO_TABLE + " ORDER BY " +
                DatabaseInfo.UNIQUE_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo info = new DatabaseInfo();
                info.setId(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.UNIQUE_ID)));
                info.setCombo(cursor.getString(cursor.getColumnIndex(DatabaseInfo.COMBO)));
                info.setPrice(cursor.getFloat(cursor.getColumnIndex(DatabaseInfo.PRICE)));

                menu.add(info);
            } while (cursor.moveToNext());
        }
        // close db connection
        db.close();

        // return menu list
        return menu;
    }

    public int UpdataPrice(Float update_price, String food_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put();
        return 1;
    }

    public boolean deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + DatabaseInfo.COMBO_TABLE);
        db.execSQL("delete from " + DatabaseInfo.FOOD_COMBO);
        db.execSQL("delete from " + DatabaseInfo.FOOD_TABLE);

        return true;

    }

    public boolean deleteCombo(String s) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DatabaseInfo.COMBO_TABLE, DatabaseInfo.COMBO + " = ?",
                new String[]{s});

        db.close();
        return true;

    }

    public int getComboCount() {
        String countQuery = "SELECT  * FROM " + DatabaseInfo.COMBO_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

    public int updateSelected(ArrayList<DatabaseInfo> databaseInfos) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        for (int i = 0; i < databaseInfos.size(); i++) {
//            System.out.println( databaseInfos.get(i).getEditTextValue().toString());
            //values.put(DatabaseInfo.COMBO, comboList.get(i).getCOMBO());
            values.put(DatabaseInfo.PRICE, databaseInfos.get(i).getEditTextValue());
            db.update(DatabaseInfo.FOOD_TABLE, values, DatabaseInfo.UNIQUE_NAME + " = ?",
                    new String[]{String.valueOf(databaseInfos.get(i).getUniqueName())});


        }
        return 1;


    }

    public List<DatabaseInfo> getComboDetails(int ID) {
        List<DatabaseInfo> ComboDetails = new ArrayList<>();

        String selectFrombothQuery = "SELECT  distinct F.price, fc.food_combo  FROM " + DatabaseInfo.FOOD_TABLE +
                " F ," + DatabaseInfo.FOOD_COMBO + " fc," + DatabaseInfo.COMBO_TABLE + " c where  f.food_combo= fc.food_combo and fc.id=" + ID;
        System.out.println(selectFrombothQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectFrombothQuery, null);
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo comboDetailsData = new DatabaseInfo();
                comboDetailsData.setUnique_name(cursor.getString(cursor.getColumnIndex(comboDetailsData.UNIQUE_NAME)));
                comboDetailsData.setPrice(cursor.getFloat(cursor.getColumnIndex(comboDetailsData.PRICE)));
                ComboDetails.add(comboDetailsData);
            } while (cursor.moveToNext());

        }
        db.close();
        return ComboDetails;
    }

    String food_combo_treat;
    int comboID;

    public OrderData getLowestPrice(ArrayList<DatabaseInfo> databaseInfos, ArrayList<String> food_name) {
        SQLiteDatabase db = this.getWritableDatabase();
        System.out.println(food_name.size());
        if(food_name.size()==1){
            String selectPrice1 = "SELECT distinct " + DatabaseInfo.PRICE + " FROM " + DatabaseInfo.FOOD_TABLE +
                    "  where " + DatabaseInfo.UNIQUE_NAME + "='" + food_name.get(0) + "'";
            System.out.println(selectPrice1);

            Cursor cursors = db.rawQuery(selectPrice1, null);
//                                            price = 0;
            if (cursors.moveToFirst()) {
                do {

                    price1 = cursors.getFloat(0);

                    System.out.println(price);
                    price =  price1 * Integer.parseInt(databaseInfos.get(0).getEditTextValue());
                } while (cursors.moveToNext());


                // price= price2+ price1;
                System.out.println(price);


            }
            return new OrderData("no Combo", price, 0);

        }
        if (food_name.size() > 0) {
            StringBuilder sb = new StringBuilder();

            for (String s : food_name) {
                sb.append(s).append(",");
            }

            food_combo_treat = sb.deleteCharAt(sb.length() - 1).toString();
        }

        System.out.println(food_combo_treat);
        String selectQuery = "SELECT  * FROM " + DatabaseInfo.COMBO_TABLE + " where " + DatabaseInfo.COMBO + " = '" + food_combo_treat + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        List<DatabaseInfo> ComboDetails = new ArrayList<>();


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                info = new DatabaseInfo();
                info.setId(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.UNIQUE_ID)));
                info.setCombo(cursor.getString(cursor.getColumnIndex(DatabaseInfo.COMBO)));
                info.setPrice(cursor.getFloat(cursor.getColumnIndex(DatabaseInfo.PRICE)));
                comboPrice = cursor.getFloat(cursor.getColumnIndex(DatabaseInfo.PRICE));
                comboID = cursor.getInt(cursor.getColumnIndex(DatabaseInfo.UNIQUE_ID));


            } while (cursor.moveToNext());

            for (int i = 0; i < databaseInfos.size(); i++) {
                for (int j = i + 1; j < databaseInfos.size(); j++) {
                    if (databaseInfos.size() == 2) {
                        if (Integer.parseInt(databaseInfos.get(i).getEditTextValue()) - Integer.parseInt(databaseInfos.get(j).getEditTextValue()) == 0) {
                            comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(i).getEditTextValue());
                            System.out.println(comboPrice);

                        } else if (Integer.parseInt(databaseInfos.get(i).getEditTextValue()) > Integer.parseInt(databaseInfos.get(j).getEditTextValue())) {
                            comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(j).getEditTextValue()) + databaseInfos.get(i).getPRICE() * (Integer.parseInt(databaseInfos.get(i).getEditTextValue()) - Integer.parseInt(databaseInfos.get(j).getEditTextValue()));
                            System.out.println(comboPrice);

                        } else {
                            comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(i).getEditTextValue()) + databaseInfos.get(j).getPRICE() * (Integer.parseInt(databaseInfos.get(j).getEditTextValue()) - Integer.parseInt(databaseInfos.get(i).getEditTextValue()));
                            System.out.println(comboPrice);

                        }


                        return new OrderData(food_combo_treat, comboPrice, comboID);
                    } else {
                        comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(j).getEditTextValue());
                        System.out.println(comboPrice);
                        return new OrderData(food_combo_treat, comboPrice, comboID);

                    }


                }
            }


        } else {


            List<DatabaseInfo> ComboDetailstemp = new ArrayList<>();

            String maxPrice = "select f." + DatabaseInfo.UNIQUE_NAME + ", max(f.price) from " + DatabaseInfo.FOOD_TABLE + " f , " + DatabaseInfo.FOOD_COMBO + " c" + " where (";

            for (int i = 0; i < databaseInfos.size(); i++) {
                if (i == 0) {
                    maxPrice += "f." + DatabaseInfo.UNIQUE_NAME + " like '%" + databaseInfos.get(i).getUniqueName();
                } else {
                    //combo += sbString.append(databaseInfos.get(i).getUniqueName()).append(",");
                    maxPrice += "%' or " + "f." + DatabaseInfo.UNIQUE_NAME + " like '%" + databaseInfos.get(i).getUniqueName();
                }

            }
            maxPrice += "%' ) and c.food_combo = f.food_combo";
            System.out.println(maxPrice);
            Cursor cursor1 = db.rawQuery(maxPrice, null);
            if (cursor1.moveToFirst()) {
                do {
                    DatabaseInfo comboDetailsData = new DatabaseInfo();
                    comboDetailsData.setUnique_name(cursor1.getString(cursor1.getColumnIndex(comboDetailsData.UNIQUE_NAME)));
                    comboDetailsData.setPrice(cursor1.getFloat(1));
                    maxPriceName = cursor1.getString(cursor1.getColumnIndex(comboDetailsData.UNIQUE_NAME));
                    maxPriceValue = cursor1.getFloat(1);
                    ComboDetailstemp.add(comboDetailsData);
                } while (cursor1.moveToNext());

            }
            for (int i = 0; i < ComboDetailstemp.size(); i++) {
                System.out.println(ComboDetailstemp.get(i).getUniqueName());
                System.out.println(ComboDetailstemp.get(i).getPRICE());

            }

            String getID = "select DISTINCT id from " + DatabaseInfo.COMBO_TABLE + " where " + DatabaseInfo.COMBO + " like '%" + ComboDetailstemp.get(0).getUniqueName() + "%' order by length(" + DatabaseInfo.COMBO + ") desc";
            System.out.println(getID);
            //String name = ComboDetailstemp.get(0).getUniqueName();
            Cursor cursor8 = db.rawQuery(getID, null);
            if (cursor8.moveToFirst()) {
                do {
                    DatabaseInfo comboDetailsData = new DatabaseInfo();
//                    String combo = cursor2.getString(cursor2.getColumnIndex(comboDetailsData.COMBO));
                    int id = cursor8.getInt(0);
                    System.out.println(id);
                    String getuniqueName = "select * from " + DatabaseInfo.FOOD_COMBO + " where " + DatabaseInfo.UNIQUE_ID + "= " + id;/*+ " and "+ DatabaseInfo.UNIQUE_NAME +" not like '%"+ ComboDetailstemp.get(0).getUniqueName() + "%'";
                    System.out.println(getuniqueName);*/
                    Cursor cursor3 = db.rawQuery(getuniqueName, null);
                    if (cursor3.moveToFirst()) {
                        do {
                            String uniqueName = cursor3.getString(0);

                            System.out.println(uniqueName);
                            compareName.add(uniqueName);


                        } while (cursor3.moveToNext());


                        // comboDetailsData.setPrice(cursor.getFloat(cursor.getColumnIndex(comboDetailsData.PRICE)));
//                ComboDetailstemp.add(comboDetailsData);
                    }
                    if (food_name.containsAll(compareName) && compareName.size() > 0) {
                        System.out.println("Common elements: " + compareName);

                        System.out.println("contains");

                        if (compareName.size() > 0) {
                            StringBuilder sb = new StringBuilder();

                            for (String s : compareName) {
                                sb.append(s).append(",");
                            }

                            food_combo_treat = sb.deleteCharAt(sb.length() - 1).toString();
                            System.out.println(food_combo_treat);
                        }


                        String selectFromComboQuery = "SELECT " + DatabaseInfo.PRICE + ", " + DatabaseInfo.UNIQUE_ID + " FROM " + DatabaseInfo.COMBO_TABLE +
                                "  where " + DatabaseInfo.COMBO + "='" + food_combo_treat + "'";
                        System.out.println(selectFromComboQuery);
                        Cursor cursor4 = db.rawQuery(selectFromComboQuery, null);
                        if (cursor4.moveToFirst()) {
                            do {
                                comboPrice = cursor4.getFloat(0);
                                comboID = cursor4.getInt(1);
                            } while (cursor4.moveToNext());

                            for (int i = 0; i < compareName.size(); i++) {
                                if (compareName.size() == 2) {

                                    for (int j = i + 1; j < compareName.size(); j++) {
                                        try {
                                            if (Integer.parseInt(databaseInfos.get(i).getEditTextValue()) - Integer.parseInt(databaseInfos.get(j).getEditTextValue()) == 0) {
                                                comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(i).getEditTextValue());
                                                System.out.println(comboPrice);

                                            } else if (Integer.parseInt(databaseInfos.get(i).getEditTextValue()) > Integer.parseInt(databaseInfos.get(j).getEditTextValue())) {
                                                comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(j).getEditTextValue()) + databaseInfos.get(i).getPRICE() * (Integer.parseInt(databaseInfos.get(i).getEditTextValue()) - Integer.parseInt(databaseInfos.get(j).getEditTextValue()));
                                                System.out.println(comboPrice);

                                            } else {
                                                comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(i).getEditTextValue()) + databaseInfos.get(j).getPRICE() * (Integer.parseInt(databaseInfos.get(j).getEditTextValue()) - Integer.parseInt(databaseInfos.get(i).getEditTextValue()));
                                                System.out.println(comboPrice);

                                            }
                                        } catch (NumberFormatException e) {
                                            e.printStackTrace();

                                        }
                                    }
                                } else {
                                    comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(i).getEditTextValue());
                                }
                            }
                            System.out.println(food_name);
                            System.out.println(compareName);
                            food_name.removeAll(compareName);
                            System.out.println(food_name);
                            for (int k = 0; k < food_name.size(); k++) {
                                String selectPrice = "SELECT distinct " + DatabaseInfo.PRICE + " FROM " + DatabaseInfo.FOOD_TABLE +
                                        "  where " + DatabaseInfo.UNIQUE_NAME + "='" + food_name.get(k) + "'";
                                System.out.println(selectPrice);

                                Cursor cursor5 = db.rawQuery(selectPrice, null);
                                if (cursor5.moveToFirst()) {
                                    do {
                                        price1 = cursor5.getFloat(0);

                                        System.out.println(price1);
                                        price1 = price + price1 * Integer.parseInt(databaseInfos.get(k).getEditTextValue());
                                        price = price1;
                                    } while (cursor5.moveToNext());


                                    // price= price2+ price1;
                                    System.out.println(price1);

                                }
                            }
                            System.out.println(comboPrice + price1);
                            return new OrderData(food_combo_treat, comboPrice + price1, comboID);

                        }

                    } else {


                        List<DatabaseInfo> ComboDetailstempNEXT = new ArrayList<>();
                        ArrayList<String> foodTemp = new ArrayList<>();
                        foodTemp = food_name;
                        System.out.println(food_name);
                        System.out.println(compareName);
                        foodTemp.remove(maxPriceName);
                        System.out.println(food_name);
                        System.out.println(foodTemp);


                        String nextmaxPrice = "select f." + DatabaseInfo.UNIQUE_NAME + ", max(f.price) from " + DatabaseInfo.FOOD_TABLE + " f , " + DatabaseInfo.FOOD_COMBO + " c" + " where (";

                        for (int i = 0; i < foodTemp.size(); i++) {
                            if (i == 0) {
                                nextmaxPrice += "f." + DatabaseInfo.UNIQUE_NAME + " like '%" + foodTemp.get(i);
                            } else {
                                //combo += sbString.append(databaseInfos.get(i).getUniqueName()).append(",");
                                nextmaxPrice += "%' or " + "f." + DatabaseInfo.UNIQUE_NAME + " like '%" + foodTemp.get(i);
                            }

                        }
                        nextmaxPrice += "%' ) and c.food_combo = f.food_combo ";
                        System.out.println(nextmaxPrice);
                        Cursor cursor7 = db.rawQuery(nextmaxPrice, null);
                        if (cursor7.moveToFirst()) {
                            do {
                                DatabaseInfo comboDetailsData7 = new DatabaseInfo();
                                comboDetailsData7.setUnique_name(cursor7.getString(cursor7.getColumnIndex(comboDetailsData7.UNIQUE_NAME)));
                                comboDetailsData7.setPrice(cursor7.getFloat(1));
                                ComboDetailstempNEXT.add(comboDetailsData7);
                            } while (cursor7.moveToNext());

                        }
                        for (int i = 0; i < ComboDetailstempNEXT.size(); i++) {
                            System.out.println(ComboDetailstempNEXT.get(i).getUniqueName());
                            System.out.println(ComboDetailstempNEXT.get(i).getPRICE());

                        }

                        getID = "select DISTINCT id from " + DatabaseInfo.COMBO_TABLE + " where " + DatabaseInfo.COMBO + " like '%" + ComboDetailstempNEXT.get(0).getUniqueName() + "%' order by length(" + DatabaseInfo.COMBO + ") desc";
                        System.out.println(getID);
                        //String name = ComboDetailstemp.get(0).getUniqueName();
                        Cursor cursor9 = db.rawQuery(getID, null);
                        if (cursor9.moveToFirst()) {
                            do {
                                DatabaseInfo comboDetailsData9 = new DatabaseInfo();
//                    String combo = cursor2.getString(cursor2.getColumnIndex(comboDetailsData.COMBO));
                                id = cursor9.getInt(0);
                                System.out.println(id);
                                getuniqueName = "select * from " + DatabaseInfo.FOOD_COMBO + " where " + DatabaseInfo.UNIQUE_ID + "= " + id;/*+ " and "+ DatabaseInfo.UNIQUE_NAME +" not like '%"+ ComboDetailstemp.get(0).getUniqueName() + "%'";
                    System.out.println(getuniqueName);*/
                                System.out.println(getuniqueName);
                                Cursor cursor10 = db.rawQuery(getuniqueName, null);
                                if (cursor10.moveToFirst()) {
                                    do {
                                        String uniqueName = cursor10.getString(0);

                                        System.out.println(uniqueName);
                                        compareName.add(uniqueName);


                                    } while (cursor10.moveToNext());


                                    // comboDetailsData.setPrice(cursor.getFloat(cursor.getColumnIndex(comboDetailsData.PRICE)));
//                ComboDetailstemp.add(comboDetailsData);
                                }
                                if (food_name.containsAll(compareName) && compareName.size() > 0) {
                                    System.out.println("Common elements: " + compareName);

                                    System.out.println("contains");

                                    if (compareName.size() > 0) {
                                        StringBuilder sb = new StringBuilder();

                                        for (String s : compareName) {
                                            sb.append(s).append(",");
                                        }

                                        food_combo_treat = sb.deleteCharAt(sb.length() - 1).toString();
                                        System.out.println(food_combo_treat);
                                    }


                                    String selectFromComboQuery1 = "SELECT " + DatabaseInfo.PRICE + ", " + DatabaseInfo.UNIQUE_ID + " FROM " + DatabaseInfo.COMBO_TABLE +
                                            "  where " + DatabaseInfo.COMBO + "='" + food_combo_treat + "'";
                                    System.out.println(selectFromComboQuery1);
                                    Cursor cursor11 = db.rawQuery(selectFromComboQuery1, null);
                                    if (cursor11.moveToFirst()) {
                                        do {
                                            comboPrice = cursor11.getFloat(0);
                                            comboID = cursor11.getInt(1);
                                        } while (cursor11.moveToNext());

                                        for (int i = 0; i < compareName.size(); i++) {
                                            if (compareName.size() == 2) {

                                                for (int j = i + 1; j < compareName.size(); j++) {
                                                    try {
                                                        if (Integer.parseInt(databaseInfos.get(i).getEditTextValue()) - Integer.parseInt(databaseInfos.get(j).getEditTextValue()) == 0) {
                                                            comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(i).getEditTextValue());
                                                            System.out.println(comboPrice);

                                                        } else if (Integer.parseInt(databaseInfos.get(i).getEditTextValue()) > Integer.parseInt(databaseInfos.get(j).getEditTextValue())) {
                                                            comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(j).getEditTextValue()) + databaseInfos.get(i).getPRICE() * (Integer.parseInt(databaseInfos.get(i).getEditTextValue()) - Integer.parseInt(databaseInfos.get(j).getEditTextValue()));
                                                            System.out.println(comboPrice);

                                                        } else {
                                                            comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(i).getEditTextValue()) + databaseInfos.get(j).getPRICE() * (Integer.parseInt(databaseInfos.get(j).getEditTextValue()) - Integer.parseInt(databaseInfos.get(i).getEditTextValue()));
                                                            System.out.println(comboPrice);

                                                        }
                                                    } catch (NumberFormatException e) {
                                                        e.printStackTrace();

                                                    }
                                                }
                                            } else {
                                                comboPrice = comboPrice * Integer.parseInt(databaseInfos.get(i).getEditTextValue());
                                            }
                                        }
                                        System.out.println(food_name);
                                        System.out.println(compareName);
                                        food_name.add(maxPriceName);

                                        food_name.removeAll(compareName);
                                        System.out.println(food_name);
                                        for (int k = 0; k < food_name.size(); k++) {
                                            String selectPrice1 = "SELECT distinct " + DatabaseInfo.PRICE + " FROM " + DatabaseInfo.FOOD_TABLE +
                                                    "  where " + DatabaseInfo.UNIQUE_NAME + "='" + food_name.get(k) + "'";
                                            System.out.println(selectPrice1);

                                            Cursor cursor12 = db.rawQuery(selectPrice1, null);
//                                            price = 0;
                                            if (cursor12.moveToFirst()) {
                                                do {

                                                    price1 = cursor12.getFloat(0);

                                                    System.out.println(price);
                                                    price1 = price + price1 * Integer.parseInt(databaseInfos.get(k).getEditTextValue());
                                                    price = price1;
                                                } while (cursor12.moveToNext());


                                                // price= price2+ price1;
                                                System.out.println(price);

                                            }
                                        }
                                        System.out.println(comboPrice + price);
                                        return new OrderData(food_combo_treat, comboPrice + price, comboID);

                                    }
                                }
                                compareName.clear();

                            }
                            while (cursor8.moveToNext());


                        }


                    }

                } while (cursor8.moveToNext());

            }
        }

        return null;
    }





    private float getLowPrice(List<DatabaseInfo> comboDetailstemp, ArrayList<String> food_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        List<DatabaseInfo> ComboDetailstemp = comboDetailstemp;
        ArrayList<String> compareName = new ArrayList<>();

        String comboID = "select * from " + DatabaseInfo.COMBO_TABLE + " where " + DatabaseInfo.COMBO + " like '%" + ComboDetailstemp.get(0).getUniqueName() + "%' order by length(" + DatabaseInfo.COMBO + ") desc";
        System.out.println(comboID);
        Cursor cursor = db.rawQuery(comboID, null);
        if (cursor.moveToFirst()) {
            do {
                DatabaseInfo comboDetailsData = new DatabaseInfo();
                String combo = cursor.getString(cursor.getColumnIndex(comboDetailsData.COMBO));
                int id = cursor.getInt(0);
                System.out.println(id);
                String getuniqueName = "select * from " + DatabaseInfo.FOOD_COMBO + " where " + DatabaseInfo.UNIQUE_ID + "= " + id;/*+ " and "+ DatabaseInfo.UNIQUE_NAME +" not like '%"+ ComboDetailstemp.get(0).getUniqueName() + "%'";
                    System.out.println(getuniqueName);*/
                Cursor cursor1 = db.rawQuery(getuniqueName, null);
                if (cursor1.moveToFirst()) {
                    do {
                        String uniqueName = cursor1.getString(0);

                        System.out.println(uniqueName);
                        compareName.add(uniqueName);


                    } while (cursor1.moveToNext());


                    // comboDetailsData.setPrice(cursor.getFloat(cursor.getColumnIndex(comboDetailsData.PRICE)));
//                ComboDetailstemp.add(comboDetailsData);
                }
                if (food_name.containsAll(compareName) && compareName.size() > 0) {
                    System.out.println("Common elements: " + compareName);

                    System.out.println("contains");
                    String combo_treat = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        combo_treat = String.join(",", compareName);
                    }
                    System.out.println(combo_treat);

                    String selectFromComboQuery = "SELECT " + DatabaseInfo.PRICE + ", " + DatabaseInfo.UNIQUE_ID + " FROM " + DatabaseInfo.COMBO_TABLE +
                            "  where " + DatabaseInfo.COMBO + "='" + combo_treat + "'";
                    Cursor cursor2 = db.rawQuery(selectFromComboQuery, null);
                    if (cursor2.moveToFirst()) {
                        do {
                            Float price2 = cursor2.getFloat(0);
                            int uid = cursor2.getInt(1);
                            if (combo_treat == food_combo_treat) {


                                price= price2;
                                System.out.println(price);
                            } else {
                                food_name.removeAll(compareName);



                                System.out.println(food_name);
                                System.out.println(food_name.size());
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                                    returnstringFood = String.join(",", food_name);
                                }
                                for (int k = 0; k < food_name.size(); k++) {
                                    String selectPrice = "SELECT distinct " + DatabaseInfo.PRICE + " FROM " + DatabaseInfo.FOOD_TABLE +
                                            "  where " + DatabaseInfo.UNIQUE_NAME + "='" + food_name.get(k) + "'";
                                    System.out.println(selectPrice);

                                    Cursor cursor3 = db.rawQuery(selectPrice, null);
                                    if (cursor3.moveToFirst()) {
                                        do {
                                            price1 = cursor3.getFloat(0);
                                            System.out.println(price1);
                                            price1 = price1+price2;
                                        } while (cursor3.moveToNext());


                                       // price= price2+ price1;
                                        System.out.println(price1);

                                    }
                                }


                            }
                        } while (cursor2.moveToNext());

                    }
                }

                compareName.clear();
            }
            while (cursor.moveToNext());
        }
        return price;
    }

    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list)
    {

        // Create a new LinkedHashSet
        Set<T> set = new LinkedHashSet<>();

        // Add the elements to set
        set.addAll(list);

        // Clear the list
        list.clear();

        // add the elements of set
        // with no duplicates to the list
        list.addAll(set);

        // return the list
        return list;
    }
    private List<DatabaseInfo> getHighestPrice(ArrayList<DatabaseInfo> databaseInfos) {
            List<DatabaseInfo> ComboDetailstemp = new ArrayList<>();

            String maxPrice = "select "+ DatabaseInfo.UNIQUE_NAME+ ", max(price) from "+ DatabaseInfo.FOOD_TABLE +" where ";

            for(int i = 0; i<databaseInfos.size(); i++){
                if (i == 0){ maxPrice+= DatabaseInfo.UNIQUE_NAME + " like '%" + databaseInfos.get(i).getUniqueName();}
                else{
                    //combo += sbString.append(databaseInfos.get(i).getUniqueName()).append(",");
                    maxPrice += "%' or "+ DatabaseInfo.UNIQUE_NAME + " like '%" + databaseInfos.get(i).getUniqueName();}

            } maxPrice += "%'";
            System.out.println( maxPrice  );
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(maxPrice, null);
            if(cursor.moveToFirst()){
                do {
                    DatabaseInfo comboDetailsData = new DatabaseInfo();
                    comboDetailsData.setUnique_name(cursor.getString(cursor.getColumnIndex(comboDetailsData.UNIQUE_NAME)));
                    comboDetailsData.setPrice(cursor.getFloat(1));
                    ComboDetailstemp.add(comboDetailsData);
                } while (cursor.moveToNext());

            }
            for(int i= 0; i<ComboDetailstemp.size(); i++){
                System.out.println(ComboDetailstemp.get(i).getUniqueName());
                System.out.println(ComboDetailstemp.get(i).getPRICE());

            }
            return ComboDetailstemp;

    }


    public OrderData getPrice(ArrayList<DatabaseInfo> databaseInfo) {
        for (int k = 0; k < databaseInfo.size(); k++) {
            String selectPrice = "SELECT distinct " + DatabaseInfo.PRICE + " FROM " + DatabaseInfo.FOOD_TABLE +
                    "  where " + DatabaseInfo.UNIQUE_NAME + "='" + databaseInfo.get(k).getUniqueName() + "'";
            System.out.println(selectPrice);
            SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor3 = db.rawQuery(selectPrice, null);
            if (cursor3.moveToFirst()) {
                do {
                    price1 = cursor3.getFloat(0);
                    System.out.println(price1);
                    price = price+ price1 *Integer.parseInt( databaseInfo.get(k).getEditTextValue());
                } while (cursor3.moveToNext());


                // price= price2+ price1;
                System.out.println(price);

            }
        }

        return new OrderData("Random Data", price, 0);

    }
}