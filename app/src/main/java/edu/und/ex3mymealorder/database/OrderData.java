package edu.und.ex3mymealorder.database;

import java.io.Serializable;

public class OrderData implements Serializable {
    public String comboname;
    public float price;
    public int id;

    public OrderData(String comboname, float price, int id) {
        this.comboname = comboname;
        this.price = price;
        this.id = id;
    }
}
