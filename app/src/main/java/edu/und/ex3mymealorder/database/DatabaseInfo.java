package edu.und.ex3mymealorder.database;

import java.io.Serializable;

public class DatabaseInfo implements Serializable {
    public static final String FOOD_TABLE = "food_table";
    public static final String COMBO_TABLE = "combo_table";
    public static final String FOOD_COMBO = "meal_table";

    public static final String UNIQUE_NAME = "food_combo";


    public static final String UNIQUE_ID = "id";
    public static final String COMBO = "combo";

    public static final String PRICE = "price";



    private int id;
    private String unique_name;
    private float price ;

    public float getTotal() {
        return total;
    }

    private float total ;

    private String combo;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    private int quantity;


    public String getEditTextValue() {
        return editTextValue;
    }

    public void setEditTextValue(String editTextValue) {
        this.editTextValue = editTextValue;
    }

    private String editTextValue;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    private boolean selected;

    public DatabaseInfo() {
    }
    public static final String CREATE_FOOD_TABLE =
            "CREATE TABLE " + FOOD_TABLE + "("
                    + UNIQUE_NAME + " VARCHAR(255) PRIMARY KEY,"
                    + PRICE + " DECIMAL(10,5) not NULL check( price  >= 0.0)"
                    + ")";

    public static final String CREATE_MEAL_TABLE =
            "CREATE TABLE " + COMBO_TABLE + "("
                    + UNIQUE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COMBO + " text,"
                    + PRICE + "  DECIMAL(8,2) not NULL check( price  >= 0.0)"

                    + ")";
    public static final String CREATE_FOOD_COMBO_TABLE =
            "CREATE TABLE " + FOOD_COMBO + "("
                    + UNIQUE_NAME + " VARCHAR(255),"

                    + UNIQUE_ID + " INTEGER,"
                    + "FOREIGN KEY (" + UNIQUE_NAME + ") REFERENCES " + FOOD_TABLE
                    +"(" + UNIQUE_NAME + ") ON DELETE CASCADE, "
                    +  "FOREIGN KEY (" + UNIQUE_ID + ") REFERENCES " + COMBO_TABLE
                    +"(" + UNIQUE_ID + ") ON DELETE CASCADE )";





    public DatabaseInfo(String name, Float price) {
        this.unique_name = name;
        this.price = price;
    }
    public DatabaseInfo(int id, String combo, Float price) {
        this.id = id;
        this.combo = combo;
        this.price = price;
    }
    public  String getUniqueName() {
        return unique_name;
    }

    public  Integer getUniqueId() {
        return id;
    }

    public  String getCOMBO() {
        return combo;
    }

    public  Float getPRICE() {
        return price;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setUnique_name(String unique_name) {
        this.unique_name = unique_name;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setCombo(String combo) {
        this.combo = combo;
    }


    public void setTotal(float comboPrice) {
        this.price = comboPrice;
    }
}
