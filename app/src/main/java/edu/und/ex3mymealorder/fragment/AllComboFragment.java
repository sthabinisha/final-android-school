package edu.und.ex3mymealorder.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import edu.und.ex3mymealorder.R;
import edu.und.ex3mymealorder.adapter.ComboAdapter;
import edu.und.ex3mymealorder.adapter.MealAdapter;
import edu.und.ex3mymealorder.database.DatabaseHelper;
import edu.und.ex3mymealorder.database.DatabaseInfo;


public class AllComboFragment extends Fragment {
    Button enterData;
    private DatabaseHelper db;
    private ComboAdapter comboAdapter;
    RecyclerView recyclerView;
    Button button_combo;

    List<DatabaseInfo> food_list = new ArrayList<DatabaseInfo>();

    public AllComboFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_combo, container, false);
        db = new DatabaseHelper(getActivity());
        recyclerView = view.findViewById(R.id.recycler_view);
       // button_combo = view.findViewById(R.id.addCombo);
        if(food_list!= null){
            food_list.clear();
        }
        food_list.addAll(db.getCombo());
        comboAdapter = new ComboAdapter(getActivity(), food_list, AllComboFragment.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(comboAdapter);
        comboAdapter.notifyDataSetChanged();
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                view.findViewById(R.id.navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.action_update:
                             DatabaseHelper helpers = new DatabaseHelper(getActivity());
                             DatabaseInfo databaseInfo;
                               int position =( (ComboAdapter)recyclerView.getAdapter()).getSelected();

                                helpers.deleteCombo(food_list.get(position).getCOMBO());
                                food_list.remove(position);
                                comboAdapter.notifyItemRemoved(position);

                                comboAdapter.notifyItemRangeChanged(position, db.getComboCount());


                                //comboAdapter.getSelected()



                                break;
                            case R.id.action_delete:
                                DatabaseHelper helpers1 = new DatabaseHelper(getActivity());
                                DatabaseInfo databaseInfo1;
                                int position1 =( (ComboAdapter)recyclerView.getAdapter()).getSelected();

                                helpers1.deleteCombo(food_list.get(position1).getCOMBO());
                                food_list.remove(position1);
                                comboAdapter.notifyItemRemoved(position1);

                                comboAdapter.notifyItemRangeChanged(position1, db.getComboCount());




                                break;


                        }
                        return true;
                    }


                });

        return view;
    }


    public void callnextFrag(Integer uniqueId) {
        Fragment fragment = new ComboDetails();

        Bundle bundle = new Bundle();
        bundle.putInt("UID", uniqueId);

        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_layout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();


    }

    @Override
    public void onResume() {
        super.onResume();
       // food_list.clear();
    }
}
