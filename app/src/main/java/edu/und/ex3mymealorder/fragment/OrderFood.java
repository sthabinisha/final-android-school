package edu.und.ex3mymealorder.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import edu.und.ex3mymealorder.R;
import edu.und.ex3mymealorder.adapter.MealAdapter;
import edu.und.ex3mymealorder.adapter.OrderAdapter;
import edu.und.ex3mymealorder.database.DatabaseHelper;
import edu.und.ex3mymealorder.database.DatabaseInfo;
import edu.und.ex3mymealorder.database.OrderData;


public class OrderFood extends Fragment {
    TextView textView;
    TextView textViewFood;
    TextView textViewID;
    RecyclerView recyclerView;
    float price;
    int id = 0;
    String comboData;
    String single;
    OrderAdapter orderAdapter;
    ArrayList<DatabaseInfo> databaseInfo = new ArrayList<>();
    DatabaseHelper db;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_order_food, container, false);
        textView = view.findViewById(R.id.price);
        textViewFood = view.findViewById(R.id.fooditems);
        textViewID = view.findViewById(R.id.foodID);
        Bundle bundle = this.getArguments();

        if (bundle != null) {
             databaseInfo = (ArrayList<DatabaseInfo>)getArguments().getSerializable("databaseInfos");
            System.out.println(databaseInfo.size());
            OrderData orderData = (OrderData) bundle.getSerializable("comboData") ;

            StringBuilder sbString = new StringBuilder("");

            for (int i = 0; i < databaseInfo.size(); i++) {
                sbString.append(databaseInfo.get(i).getUniqueName()).append(",");
            }
            single = sbString.toString();
            single = single.substring(0, single.length() - 1);
            try {
                price = orderData.price;
                id = orderData.id;
                comboData = orderData.comboname;
                textView.setText(price +" is the lowest price");
                textViewFood.setText("Your combo order item is "+comboData );
                if(id != 0) {
                    textViewID.setText(id+"");
                    makeTextViewHyperlink(textViewID);

                }else{
                    textViewID.setText("no ID");
                }
            } catch (Exception e) {
                e.printStackTrace();
                DatabaseHelper helper = new DatabaseHelper(getActivity());
                orderData = helper.getPrice(databaseInfo);
                price = orderData.price;
                id = orderData.id;
                comboData = orderData.comboname;
                textView.setText(price +" is the lowest price");
                textViewFood.setText("Your order item is "+single );
                if(id != 0) {
                    textViewID.setText(id+"");
                    makeTextViewHyperlink(textViewID);

                }else{
                    textViewID.setText("");
                }

            }

          textViewID.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  Fragment fragment = new ComboDetails();

                  Bundle bundle = new Bundle();
                  bundle.putInt("UID", id);

                  fragment.setArguments(bundle);
                  FragmentManager fragmentManager = getFragmentManager();
                  FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                  fragmentTransaction.replace(R.id.content_layout, fragment);
                  fragmentTransaction.addToBackStack(null);
                  fragmentTransaction.commit();
              }
          });


//


        }





        return view;

    }

    public static void makeTextViewHyperlink( TextView tv ) {

        SpannableStringBuilder ssb = new SpannableStringBuilder( );

        ssb.append( tv.getText( ) );

        ssb.setSpan( new URLSpan("#"), 0, ssb.length(),

                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );

        tv.setText( ssb, TextView.BufferType.SPANNABLE );

    }


}
