package edu.und.ex3mymealorder.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import edu.und.ex3mymealorder.R;
import edu.und.ex3mymealorder.adapter.MealAdapter;
import edu.und.ex3mymealorder.database.DatabaseHelper;
import edu.und.ex3mymealorder.database.DatabaseInfo;
import edu.und.ex3mymealorder.database.OrderData;


public class AllFoodFragment extends Fragment {
    private MealAdapter mealAdapter;
    private DatabaseHelper db;

    RecyclerView recyclerView;
    List<DatabaseInfo> food_list = new ArrayList<>();
    AlertDialog.Builder builder;
    private String m_Text = "";
    static String[] c;
    static int count = 0;
    ArrayList<String> food_name = new ArrayList<>();
    static ArrayList<String> food_combination = new ArrayList<>();
    String price, food_combo, comboId;
    OrderData orderData;



    public AllFoodFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View view = inflater.inflate(R.layout.fragment_all_food_feagment, null);
        db = new DatabaseHelper(getActivity());
        recyclerView = view.findViewById(R.id.recycler_view);
        if(food_list!= null){
            food_list.clear();
        }
        food_list.addAll(db.getMenu());
        mealAdapter = new MealAdapter(getActivity(), food_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mealAdapter);
        mealAdapter.notifyDataSetChanged();

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                view.findViewById(R.id.navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.action_Entermenu:
                                builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Select from the option");
                                builder.setIcon(R.drawable.ic_my_icon);
                                builder.setPositiveButton("Enter Food items",
                                        new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog, int id)
                                            {



                                                Fragment fragment = new EnterDataFragment();
                                                FragmentManager fragmentManager = getFragmentManager();
                                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                fragmentTransaction.replace(R.id.content_layout, fragment);
                                                fragmentTransaction.addToBackStack(null);
                                                fragmentTransaction.commit();

                                                dialog.cancel();
                                            }
                                        });

                                builder.setNeutralButton("Update Price",
                                        new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog, int id)
                                            {
                                                DatabaseHelper helper = new DatabaseHelper(getActivity());

                                                ArrayList<DatabaseInfo> databaseInfos =( (MealAdapter)recyclerView.getAdapter()).getSelectedList();
                                                helper.updateSelected(databaseInfos);
                                                dialog.cancel();

                                            }
                                        });

                                builder.setNegativeButton("Order",
                                        new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog, int id)
                                            {

                                                DatabaseHelper helper = new DatabaseHelper(getActivity());

                                                ArrayList<DatabaseInfo> databaseInfos =( (MealAdapter)recyclerView.getAdapter()).getSelectedList();
                                                for(int j = 0; j< databaseInfos.size(); j++){
                                                   food_name.add(databaseInfos.get(j).getUniqueName());
                                            }



                                               orderData = helper.getLowestPrice(databaseInfos, food_name);

                                                Fragment fragment = new OrderFood();

                                                Bundle bundle = new Bundle();


                                                bundle.putSerializable("databaseInfos", databaseInfos);
                                                bundle.putSerializable("comboData", orderData);

                                                fragment.setArguments(bundle);
                                                FragmentManager fragmentManager = getFragmentManager();
                                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                                fragmentTransaction.replace(R.id.content_layout, fragment);
                                                fragmentTransaction.addToBackStack(null);
                                                fragmentTransaction.commit();

                                                dialog.cancel();
                                            }
                                        });
                                builder.create().show();


                                break;
                            case R.id.action_EnterCombo:

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle("Price");

                                final EditText input = new EditText(getActivity());
                                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_NUMBER);
                                builder.setView(input);

                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        m_Text = input.getText().toString();
                                        DatabaseHelper helper = new DatabaseHelper(getActivity());
                                        ArrayList<DatabaseInfo> databaseInfos =( (MealAdapter)recyclerView.getAdapter()).getSelectedList();

                                        helper.insertComboMeal(databaseInfos, m_Text);
                                        try {
                                            System.out.println(databaseInfos.size());
                                            Fragment fragment1 = new AllComboFragment();
                                            FragmentManager fragmentManager1 = getFragmentManager();
                                            FragmentTransaction fragmentTransaction1 = fragmentManager1.beginTransaction();
                                            fragmentTransaction1.replace(R.id.content_layout, fragment1);
                                            fragmentTransaction1.addToBackStack(null);
                                            fragmentTransaction1.commit();
                                        }catch (Exception e){
                                            Toast.makeText(getActivity(), "No Data :)", Toast.LENGTH_SHORT).show();
                                            e.printStackTrace();
                                        }

                                    }
                                });
                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });

                                builder.show();

                                //adapter.notifyDataSetChanged();




                                break;


                        }
                        return true;
                    }


                });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
       // food_list.clear();
    }




    static void swap(int pos1, int pos2)
    {
        String temp = c[pos1];
        c[pos1] = c[pos2];
        c[pos1] = temp;
    }
    public  static void permutation(int start)
    {
        if (start != 0)
        {
            for (int i = 0; i < start; i++){
                String s =c[i]+",";
                if(!food_combination.contains(s.substring(0, s.length() - 1)))
                    food_combination.add(s.substring(0, s.length() - 1));

                }
        }

        for (int i = start; i < c.length; i++)
        {
            //swap(start, i);
            permutation(start +1);
            swap(start, i);
        }
    }

}
