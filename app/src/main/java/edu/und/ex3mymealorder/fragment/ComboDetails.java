package edu.und.ex3mymealorder.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import edu.und.ex3mymealorder.R;
import edu.und.ex3mymealorder.adapter.DetailsAdapter;
import edu.und.ex3mymealorder.adapter.MealAdapter;
import edu.und.ex3mymealorder.database.DatabaseHelper;
import edu.und.ex3mymealorder.database.DatabaseInfo;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComboDetails extends Fragment {
    private List<DatabaseInfo> databaseInfos = new ArrayList<>();
    DatabaseHelper db;
    RecyclerView recyclerView;
    DetailsAdapter mealAdapter;
    int myInt;



    public ComboDetails() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_combo_details, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
             myInt = bundle.getInt("UID");
        }
        db = new DatabaseHelper(getActivity());
        recyclerView = view.findViewById(R.id.recycler_view);

        if(databaseInfos!= null){
            databaseInfos.clear();
        }
        databaseInfos.addAll(db.getComboDetails(myInt));
        mealAdapter = new DetailsAdapter(getActivity(), databaseInfos);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mealAdapter);
        return view;
    }

}
